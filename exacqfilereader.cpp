#include "exacqfilereader.h"
#include "exacqfiles.h"
#include <QDebug>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QTextCodec>

ExacqFileReader::ExacqFileReader(QObject* parent, QString path)
    : QObject(parent)
{
    //seperate paths by ';'
    this->path = path;
}

void ExacqFileReader::execSearch()
{
    QHash<QString, QString> names;
    QStringList paths = path.split(";");
    //iterate through directories
    for (int c = 0; c < paths.count(); c++) {
        path = paths[c];
        //read searchdir for files that end in .ps and get camera names from data
        if (this->path != "") {
            int icount = 0;
            QDirIterator it(path, QStringList() << "*.ps", QDir::Files, QDirIterator::Subdirectories);
            while (it.hasNext()) {
                icount++;
                QString p = it.next();
                QFileInfo psFileI(p);
                ExacqFiles f;
                //set file properties
                f.path = p;
                f.date = psFileI.lastModified();
                f.size = psFileI.size();
                f.cameraId = psFileI.baseName().right(8);
                //if we already known the camera name based on ID
                if (names.contains(psFileI.baseName().right(8))) {
                    f.cameraName = names[psFileI.baseName().right(8)];
                } else {
                    //open the file and read 32 bytes starting at byte 148
                    //this isn't very fast so we limit it's usage
                    QFile file(p);
                    if(!file.open(QIODevice::ReadOnly | QIODevice::Unbuffered))
                    {
                        emit resultError("Unable to read file: " + p);
                        return;
                    }
                    file.seek(148);
                    QByteArray bytes = file.read(32);
                    f.cameraName = QString(bytes);
                    names[psFileI.baseName().right(8)] = QString(bytes);
                    file.close();
                }
                //load up the output
                fileList.append(f);
            }
        }
    }
    //emit results back to main thread
    emit resultReady(fileList);
}
