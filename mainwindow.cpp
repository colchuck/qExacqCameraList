#include "mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QHash>
#include <QHeaderView>
#include <QListView>
#include <QMovie>
#include <QThread>
#include <QTreeView>
#include <QtCharts/QChartView>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>

#include "customtablewidgetitem.h"
#include "exacqfilereader.h"
#include "exacqfiles.h"
#include "filepickerdiag.h"
#include "ui_mainwindow.h"

using namespace QtCharts;

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    // window setup
    ui->setupUi(this);
    QIcon ico = QIcon("icon.ico");
    setWindowIcon(ico);
    setWindowTitle("qExacqCameraList");
    ui->lineEdit->setEnabled(false);
    ui->calcBtn->setEnabled(false);
    ui->resetBtn->setEnabled(false);
    ui->ExportBtn->setEnabled(false);
    // gif setup
    QMovie* movie = new QMovie(":/res/loader.gif");
    movie->start();
    ui->loading->setMovie(movie);
    ui->loading->hide();
    // table setup
    qRegisterMetaType<QList<ExacqFiles>>("QList<ExacqFiles>");
    table = ui->tableWidget;
    table->setColumnCount(5);
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table->verticalHeader()->setVisible(false);
    QHeaderView* header = table->horizontalHeader();
    header->setStretchLastSection(true);
    table->setColumnWidth(0, 300);
    table->setColumnWidth(1, 85);
    table->setColumnWidth(2, 85);
    table->setColumnWidth(3, 85);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->sortByColumn(2, Qt::DescendingOrder);
    //connect(ui->actionAbout,&QAction::triggered,MainWindow::on_AboutAction_triggered());
    // var initialization
    paths = new QStringList();
    e = new ExacqFileReader(nullptr,nullptr);
}

void MainWindow::cellSelected(int row, int col)
{
    //handles keyboard movements and relays to chart
    if (row >= 0) {
        for (int i = 0; i < series->slices().size(); i++) {
            if (series->slices().at(i)->label() == table->item(row, 0)->text()) {
                sliceClicked(series->slices().at(i));
            }
        }
    }
}

void MainWindow::cellChanged(int row, int col, int r, int c)
{
    //handles keyboard movements and relays to chart
    cellSelected(row, 0);
}

void MainWindow::on_browseBtn_clicked()
{
    // load the multi directory selecting dialog
    QStringList* tempPaths = new QStringList(*paths);
    FilePickerDiag* d = new FilePickerDiag(nullptr, tempPaths);
    int result = d->exec();
    if (result == 1) {
        paths = tempPaths;
        ui->lineEdit->setText(paths->join(";"));
        // make sure there is a least one path
        if (paths->count() > 0) {
            ui->calcBtn->setEnabled(true);
        } else {
            ui->calcBtn->setEnabled(false);
        }
        this->path = paths->join(";");
    }
}

void MainWindow::on_calcBtn_clicked()
{
    //prep thread/ui and get started
    e = new ExacqFileReader(nullptr,this->path);
    e->moveToThread(&workerThread);
    connect(&workerThread, &QThread::finished, e, &QObject::deleteLater);
    connect(this, &MainWindow::operate, e, &ExacqFileReader::execSearch);
    connect(e, &ExacqFileReader::resultReady, this, &MainWindow::handleResults);
    workerThread.start();
    ui->browseBtn->setEnabled(false);
    ui->calcBtn->setEnabled(false);
    ui->statusText->setText("Collecting Data...");
    emit operate();
    ui->loading->show();
}

void MainWindow::on_actionAbout_triggered(){
    //show about notice
    QString compilationTime = QString("%1").arg(__DATE__);
    QMessageBox::information(this,"About", "Licenced under the GNU General Public License V3.0\nBuilt on QT5\nBuild Date: "+compilationTime+"\nAuthor: Colchuck");
}

void MainWindow::on_resetBtn_clicked()
{
    //reset gui and make sure we don't have any leaks
    ui->calcBtn->setEnabled(false);
    ui->browseBtn->setEnabled(true);
    ui->ExportBtn->setEnabled(false);
    ui->statusText->setText("Idle");
    ui->lineEdit->setText("");
    table->disconnect();
    table->model()->removeRows(0, table->rowCount());
    //deconstruct series to avoid segfault
    if(ui->verticalLayout->count() != 0){
        series->slices().clear();
        series->clear();
        series->disconnect();
        chart->removeSeries(series);
        delete series;
        QLayoutItem* w = ui->verticalLayout->takeAt(0);
        delete w->widget();
    }
    table->setRowCount(0);
    ui->resetBtn->setEnabled(false);
    delete e;
    paths->clear();
}

void MainWindow::sliceClicked(QPieSlice* slice)
{
    //when a slice is clicked on the chart, find the table row
    //show label and pop out slice wilst hiding old ones
    for (int i = 0; i < series->slices().size(); i++) {
        series->slices().at(i)->setExploded(false);
        series->slices().at(i)->setLabelVisible(false);
    }
    slice->setExploded(true);
    slice->setLabelVisible(true);
    for (int i = 0; i < table->rowCount(); i++) {
        if (table->item(i, 0)->text() == slice->label()) {
            table->selectRow(i);
        }
    }
}

void MainWindow::on_ExportBtn_clicked()
{
    // get the save file name and create the CSV string.
    QString fileN = QFileDialog::getSaveFileName(
        this, tr("Save File"), QDir::homePath(), tr("CSV Files(*.csv)"));
    if (fileN == "")
        return;
    QFile file(fileN);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        // text stream csv to file
        QTextStream stream(&file);
        for (int c = 0; c < csvOut->count(); c++) {
            stream << "\"" << csvOut->at(c).at(0) << "\",\"" << csvOut->at(c).at(1)
                   << "\",\"" << csvOut->at(c).at(2) << "\",\"" << csvOut->at(c).at(3)
                   << "\",\"" << csvOut->at(c).at(4) << "\"\n";
        }
        file.close();
    } else {
        handleFailure("Cannot export file, check write permissions.");
    }
}

void MainWindow::handleFailure(QString msg){
    //display an error message
    QMessageBox::warning(this,"Error!",msg);
    //on_resetBtn_clicked();
}

void MainWindow::handleResults(const QList<ExacqFiles>& data)
{
    // get the results back from the worker thread
    ui->statusText->setText("Calculating Statistics...");
    QHash<QString, QDateTime> earliest;
    QHash<QString, qint64> cameraSizes;
    QHash<QString, QString> cameraIds;
    qint64 totalSize = 0;
    // loop through each object and sum up the results
    for (int i = 0; i < data.size(); i++) {
        totalSize += data.at(i).size;
        cameraSizes[data.at(i).cameraName] += data.at(i).size;
        cameraIds[data.at(i).cameraName] = data.at(i).cameraId;
        // search for earliest date on timestamp by camera
        if (!earliest.contains(data.at(i).cameraName)) {
            earliest[data.at(i).cameraName] = data.at(i).date;
        } else {
            if (earliest[data.at(i).cameraName] > data.at(i).date) {
                earliest[data.at(i).cameraName] = data.at(i).date;
            }
        }
    }
    //setup series
    QList<QString> l = cameraSizes.keys();
    series = new QPieSeries();
    for (int i = 0; i < l.size(); i++) {
        series->append(l[i], (cameraSizes[l[i]]));
    }
    series->setLabelsVisible(false);
    //setup chart
    chart = new QChart();
    connect(series, &QPieSeries::clicked, this, &MainWindow::sliceClicked);
    chart->removeAllSeries();
    chart->addSeries(series);
    chart->legend()->hide();
    chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    if (ui->verticalLayout->count() == 0) {
        ui->verticalLayout->addWidget(chartView);
    }
    ui->statusText->setText("Results Loaded");
    //setup table
    table->verticalHeader()->setDefaultSectionSize(15);
    table->setRowCount(l.size());
    table->setSortingEnabled(false);
    QFont fnt; //configure if we need different fonts
    // fnt.setPointSize(9);
    csvOut = new QList<QList<QString>>;
    //populate table
    for (int j = 0; j < l.size(); j++) {
        QList<QString> row;
        // name
        CustomTableWidgetItem* item0 = new CustomTableWidgetItem();
        item0->setText(l[j]);
        item0->setFont(fnt);
        table->setItem(j, 0, item0);
        row.append(l[j]);
        // id
        CustomTableWidgetItem* item4 = new CustomTableWidgetItem();
        item4->setText(cameraIds[l[j]]);
        item4->setFont(fnt);
        table->setItem(j, 1, item4);
        row.append(cameraIds[l[j]]);
        // size
        CustomTableWidgetItem* item1 = new CustomTableWidgetItem();
        item1->setText(QLocale(QLocale::English).toString(cameraSizes[l[j]] / 1024 / 1024) + "MB");
        item1->setTextAlignment(Qt::AlignRight);
        table->setItem(j, 2, item1);
        item1->setFont(fnt);
        row.append(QLocale(QLocale::English).toString(cameraSizes[l[j]] / 1024 / 1024) + "MB");
        // percentage
        CustomTableWidgetItem* item3 = new CustomTableWidgetItem();
        item3->setText(QString::number((float(cameraSizes[l[j]]) / (float(totalSize)) * 100), 'f', 2) + "%");
        item3->setFont(fnt);
        item3->setTextAlignment(Qt::AlignRight);
        table->setItem(j, 3, item3);
        table->resizeRowToContents(j);
        row.append(QString::number((float(cameraSizes[l[j]]) / (float(totalSize)) * 100), 'f', 2) + "%");
        // oldest
        CustomTableWidgetItem* item2 = new CustomTableWidgetItem();
        item2->setText(earliest[l[j]].toString("MM/dd/yyyy h:mm AP"));
        item2->setTextAlignment(Qt::AlignRight);
        table->setItem(j, 4, item2);
        item2->setFont(fnt);
        row.append(earliest[l[j]].toString("MM/dd/yyyy h:mm AP"));

        //add row to csv export var
        csvOut->append(row);
    }
    table->setSortingEnabled(true);
    //set the colors - mostly for windows gui
    QPalette p = palette();
    p.setColor(QPalette::Inactive, QPalette::Highlight, p.color(QPalette::Active, QPalette::Highlight));
    setPalette(p);
    //make sure we don't have any lingering connections and reconnect chart
    table->disconnect();
    connect(table, SIGNAL(cellClicked(int, int)), this, SLOT(cellSelected(int, int)));
    connect(table, &QTableWidget::currentCellChanged, this, &MainWindow::cellChanged);
    if (table->rowCount() > 0) {
        table->selectRow(0);
    }
    ui->resetBtn->setEnabled(true);
    ui->ExportBtn->setEnabled(true);
    ui->loading->hide();
}

MainWindow::~MainWindow()
{
    // make sure the worker is dead before closing
    workerThread.terminate();
    workerThread.wait();
    delete ui;
}
