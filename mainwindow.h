/***********************/
// Main window class for the qExacqCameraList application
// The basic premise is for gather metrics on exacq's stored data
// between various directories or hard disks.
//
/***********************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCharts/QChartView>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>
#include <QChart>
#include <QMainWindow>
#include "exacqfiles.h"
#include <QThread>
#include <QTableWidget>
#include <QtCharts/QPieSlice>
#include <QChartView>
#include "exacqfilereader.h"
#include <QMessageBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ExacqFileReader *e;
    QString path;
    QtCharts::QPieSeries *series;
    QTableWidget * table;
    QtCharts::QChart *chart;
    QtCharts::QChartView *chartView;
    ~MainWindow();

public slots:
    void handleResults(const QList<ExacqFiles> &) ;
    void handleFailure(QString msg);
    void sliceClicked(QtCharts::QPieSlice * slice);
    void on_actionAbout_triggered();

signals:
    void operate();

private slots:
    void on_browseBtn_clicked();
    void on_calcBtn_clicked();
    void cellSelected(int row, int col);
    void cellChanged(int row, int col, int r , int c);
    void on_resetBtn_clicked();
    void on_ExportBtn_clicked();

private:
    Ui::MainWindow *ui;
    QThread workerThread;
    QStringList *paths;
    QList<QList<QString>> * csvOut;

};

#endif // MAINWINDOW_H
