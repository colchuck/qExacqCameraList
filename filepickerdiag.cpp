#include "filepickerdiag.h"
#include <QDebug>
#include <QFileDialog>
#include <QStringListModel>

FilePickerDiag::FilePickerDiag(QDialog* parent, QStringList* paths)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    //window properties
    ui->setupUi(this);
    this->setModal(true);
    QIcon ico = QIcon("icon.ico");
    setWindowIcon(ico);
    setWindowTitle("Search Directories");
    //populate/setup listview model
    this->list = paths;
    model = new QStringListModel(this);
    model->setStringList(*list);
    ui->DirList->setModel(model);
}

void FilePickerDiag::on_AddNewBtn_clicked()
{
    //get the directory name
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dir == "")
        return;
    if(!list->contains(dir,Qt::CaseInsensitive)){
        int row = model->rowCount();
        model->insertRows(row, 1);
        list->append(dir);
        model->setStringList(*list);
    }
}

void FilePickerDiag::on_RemoveBtn_clicked()
{
    //find the selected row and remove it from the list
    list->removeAt(ui->DirList->selectionModel()->selectedIndexes().first().row());
    model->setStringList(*list);
}

void FilePickerDiag::on_ClearBtn_clicked()
{
    //rset the list
    list->clear();
    model->setStringList(*list);
}
