#include "customtablewidgetitem.h"
#include <QCollator>
#include <QDateTime>
#include <QDebug>
#include <QRegExp>

// this class exists simply to the user sorting ablities within the table. It
// just overrides the < operator.
bool CustomTableWidgetItem::operator<(const QTableWidgetItem& other) const
{
    QRegExp re("[0-9]{1,2}/[0-9]{1,2}/[0-9]{4} [0-9]{1,2}:[0-9]{1,2} [A,P]M");
    QString text = this->text();
    QString otherText = other.text();
    if (re.exactMatch(text)) {
        // sorting of oldest dates
        return (QDateTime::fromString(text, "MM/dd/yyyy h:m AP") < QDateTime::fromString(otherText, "MM/dd/yyyy h:m AP"));
    } else if (other.text()[other.text().length() - 1] == '%' && other.text().contains(".")) {
        // sorting of percentage usage
        QString value = other.text();
        QString curr = this->text();
        value.remove("%");
        curr.remove("%");
        return (curr.toFloat() < value.toFloat());
    } else if (other.text()[other.text().length() - 1] == "B" && other.text()[other.text().length() - 2] == "M") {
        // sorting of file sizes
        QString value = other.text();
        QString curr = this->text();
        value.remove("MB");
        curr.remove("MB");
        curr.remove(",");
        value.remove(",");
        return (curr.toFloat() < value.toFloat());
    } else {
        // this is a lazy sorting of camera names, but it works...
        QCollator collator;
        collator.setNumericMode(true);
        // return(this->text()[0]< other.text()[0]);
        return collator.compare(text, otherText) < 0;
    }
}
