/***********************/
// This class is a custom directory selecting dialog that allows
// for multiple directories to be added or removed
//
//
/***********************/
#ifndef FILEPICKERDIAG_H
#define FILEPICKERDIAG_H

#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QStringListModel>
#include "ui_filepickerdiag.h"
namespace UI{
    class FilePickerDiag;
}

class FilePickerDiag : public QDialog
{
    Q_OBJECT
public:
    explicit FilePickerDiag(QDialog *parent = nullptr,QStringList * paths = new QStringList);
private:
    Ui::Dialog *ui;
    QStringListModel * model;
    QStringList * list;
signals:

private slots:
    void on_AddNewBtn_clicked();
    void on_RemoveBtn_clicked();
    void on_ClearBtn_clicked();
};

#endif // FILEPICKERDIAG_H
