/***********************/
// This class is used by exacqFileReader as a data structure to store
// the gathered information.
//
//
/***********************/
#ifndef EXACQFILES_H
#define EXACQFILES_H

#include <QDateTime>
#include <QObject>

class ExacqFiles : public QObject {
    Q_OBJECT
public:
    ExacqFiles(const ExacqFiles& other);
    ExacqFiles& operator=(const ExacqFiles& other);
    explicit ExacqFiles(QObject* parent = nullptr);
    QString path;
    qint64 size;
    QDateTime date;
    QString cameraName;
    QString cameraId;

signals:

public slots:
};

#endif // EXACQFILES_H
