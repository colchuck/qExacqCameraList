/***********************/
// This class allows for custom sorting rules within the QTableWidget columns
//
//
//
/***********************/
#ifndef CUSTOMTABLEWIDGETITEM_H
#define CUSTOMTABLEWIDGETITEM_H
#include <QTableWidgetItem>

class CustomTableWidgetItem : public QTableWidgetItem
{

public:
    bool operator< (const QTableWidgetItem &other) const;
};

#endif // CUSTOMTABLEWIDGETITEM_H
