# qExacqCameraList

This software application is a multiplaform solution for sourcing camera disk usage. 
It allows users to select a drive or directory and see a list of cameras and how much space 
each one uses. Supports multiple devices and exporting data in CSV format. 

## Dependancies

* Qt5

## Tested Platforms

* Linux 
* Windows 7/8/8.1/10/2012R2+

## Screenshot

![](screenshot-qecl.png)
