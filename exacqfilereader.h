/***********************/
// This class reads through exacqVision's stored files
// and returns the data needed to calculate metrics.
//
//
/***********************/
#ifndef EXACQFILEREADER_H
#define EXACQFILEREADER_H

#include <QObject>
#include "exacqfiles.h"

class ExacqFileReader : public QObject
{
    Q_OBJECT
public:
    explicit ExacqFileReader(QObject *parent = nullptr, QString path = "");
    QString path;
    QList<ExacqFiles> fileList;

public slots:
    void execSearch();

signals:
    void resultReady(const QList<ExacqFiles> &);
    void updateProg(int);
    void resultError(const QString);


};

#endif // EXACQFILEREADER_H
