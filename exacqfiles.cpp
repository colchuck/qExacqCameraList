#include "exacqfiles.h"

ExacqFiles::ExacqFiles(QObject* parent)
    : QObject(parent)
{
}

ExacqFiles::ExacqFiles(const ExacqFiles& other)
{
    path = other.path;
    size = other.size;
    date = other.date;
    cameraId = other.cameraId;
    cameraName = other.cameraName;
}
